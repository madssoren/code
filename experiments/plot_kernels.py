import matplotlib.pyplot as plt
import learning.nn as n

def plot_theta_kernel(networks):
    plt.figure(1)
    col = 0
    for network in networks:
        row = 0
        col += 1
        for kernel in network.theta_kernels:
            plt.subplot(network.channels,len(networks),row*len(networks)+col)
            plt.imshow(kernel)
            plt.gray()
            plt.axis('off')
            row +=1
    plt.show()

def load_nns(fname, samplesize=1000, num_epochs=100, step_size=10, start_epoch=0):
    networks = []
    for i in range(start_epoch, num_epochs, step_size):
        networks.append(n.NeuralNet(fname + '_' + str(samplesize) + '_' + str(i)))

    return networks
