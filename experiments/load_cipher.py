import learning.mnist as m
import learning.nn as n
import crypto.gentry_cipher as g
import crypto.fhe as e
import utility.change_img as c
import utility.type_funcs as tf
import random as r
import time as t

# Init network
kern = [0.1*r.gauss(0.5, 1) for i in range(0,810)]
out = [r.gauss(0,0.01) for i in range(0,10000)]
rnd = kern+out
nn = n.NeuralNet((28,28), ((9,9), 10, (2,2)), 10, lambda : rnd.pop(0))

n = 2
train_data = m.load_data('data/train-images-idx3-ubyte', 'data/train-labels-idx1-ubyte', n)
test_data = m.load_data('data/t10k-images-idx3-ubyte', 'data/t10k-labels-idx1-ubyte', n)

num_int = 11
num_frac = 6

# Init crypto
l = 7
a = 1
b = 2
cipher = g.GentryCipher(l, a, b)

(pk, sk) = cipher.keygen()
fhe = e.Fhe(cipher)
sk_e = fhe.enc_list(pk, sk)

# Load
type_func = tf.Cipher_funcs(num_int, num_frac, fhe, pk, sk_e)
test_data_fp = c.change_data(test_data, type_func)
nn.load('a001_60000_4')
nn.change_type(type_func)

def train(epochs = 1,alpha = 0.001):
    nn.train(train_data,epochs,alpha,'vars')

def pred():
    return nn.test(test_data)

def fp_pred():
    start = t.process_time()
    res =  nn.feed_forward(test_data_fp[0][0], True)
    stop = t.process_time()
    print(f'Running time: {stop-start}\n')
    return res

def print_res(res):
    r_sum = 0
    for r in res:
        num = r[0].to_int()
        denum = r[1].to_int()
        print(f'{num} / {denum} = {num/denum}')
        r_sum += num/denum

    print(f'sums to {r_sum}')

def dump(x):
    for i in x:
        for j in i:
            print(j.to_float())
