import crypto.fhe as f
import crypto.gentry_cipher as g
import pickle as p
import time


def make_timings():
    # fixture cipher params make it too slow
    l = 9
    a = 1
    b = 2
    cipher = g.GentryCipher(l, a, b)

    (pk, sk) = cipher.keygen()
    fhe = f.Fhe(cipher)
    sk_e = fhe.enc_list(pk, sk)

    timings = []
    for i in range(0, 30):
        print(i)
        c1 = cipher.enc(pk, 0)

        
        t1 = time.process_time()
        c1_new = fhe.recrypt(pk, sk_e, c1)

        t2 = time.process_time()
        c3 = cipher.mul(pk, c1_new, c1_new)

        t3 = time.process_time()
        c3 = cipher.add(pk, c1_new, c1_new)

        t4 = time.process_time()

        timings.append((t4-t3, t3-t2, t2-t1))

    return timings


if (__name__ == '__main__'):
    timings = make_timings()
    with open('low_add_mul', 'wb') as f:
            p.dump(timings,f)


