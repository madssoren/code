import learning.nn as nn
import pickle as p
import time
import utility.type_funcs as tf
import crypto.fhe as fh
import crypto.gentry_cipher as g

def dec_fp(x,tf,sk,fhe):
    res = tf.init(0)
    res.value = fhe.dec_list(sk,x.value)
    return res

def time1():
    num_int= 3
    num_frac = 1
    l = 9
    a = 1
    b = 2
    cipher = g.GentryCipher(l, a, b)
    (pk, sk) = cipher.keygen()
    fhe = fh.Fhe(cipher)
    sk_e = fhe.enc_list(pk, sk)

    tf_plain = tf.Fp_funcs(num_int,num_frac)
    tf_cipher = tf.Cipher_funcs(num_int,num_frac,fhe,pk,sk_e)

    #Plaintexts
    pt15p = tf_plain.init(1.5)
    pt20p = tf_plain.init(2)

    #Ciphertexts
    ct15p = tf_cipher.init(1.5)
    ct20p = tf_cipher.init(2)

    # add
    pt_add = pt15p + pt20p
    ct_add = ct15p + ct20p
    add_pt = pt_add.to_float()
    add_ct = dec_fp(ct_add, tf_plain,sk,fhe).to_float()
    with open('correct_add', 'wb') as f:
        p.dump(('add', add_pt, add_ct), f)

    # mul
    pt_mul = pt15p * pt15p
    ct_mul = ct15p * ct15p

    mul_pt = pt_mul.to_float()
    mul_ct = dec_fp(ct_mul, tf_plain, sk,fhe).to_float()
    with open('correct_mul', 'wb') as f:
        p.dump(('mul', mul_pt, mul_ct), f)

    # max_zero
    pt_max = tf_plain.max_zero(pt15p)
    ct_max = tf_cipher.max_zero(ct15p)

    max_pt = pt_max.to_float()
    max_ct = dec_fp(ct_max, tf_plain, sk,fhe).to_float()
    with open('correct_max', 'wb') as f:
        p.dump(('max', max_pt, max_ct), f)

    # mean
    pt_mean = tf_plain.mean([pt15p, pt20p])
    ct_mean = tf_cipher.mean([ct15p, ct20p])

    mean_pt = pt_mean.to_float()
    mean_ct = dec_fp(ct_mean, tf_plain, sk,fhe).to_float()
    with open('correct_mean', 'wb') as f:
        p.dump(('mean', mean_pt, mean_ct), f)

def time2():
    num_int= 2
    num_frac = 1
    l = 9
    a = 1
    b = 2
    cipher = g.GentryCipher(l, a, b)
    (pk, sk) = cipher.keygen()
    fhe = fh.Fhe(cipher)
    sk_e = fhe.enc_list(pk, sk)

    tf_plain = tf.Fp_funcs(num_int,num_frac)
    tf_cipher = tf.Cipher_funcs(num_int,num_frac,fhe,pk,sk_e)

    # exp
    pt15p = tf_plain.init(1.5)
    ct15p = tf_cipher.init(1.5)

    pt_exp = tf_plain.exp(pt15p)
    ct_exp = tf_cipher.exp(ct15p)

    exp_pt = pt_mean.value
    exp_ct = fhe.dec_list(sk, ct_exp.value)

    with open('correct_exp', 'wb') as f:
        p.dump(('exp', exp_pt, exp_ct), f)

if (__name__ == '__main__'):
    time1()
    time2()
