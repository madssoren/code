import learning.nn as n
import learning.mnist as m
import time as t
import utility.change_img as c
import utility.type_funcs as tf
import pickle as p
import utility.fp_res_to_float as ftf
import sys
import multiprocessing as mp


    

def run():
    imgs = [3, 8, 9, 20, 28]
    int_bit = 12 #12
    frac_bit = 9 #9
    num_img = 30

    td = m.load_data('data/t10k-images-idx3-ubyte', 'data/t10k-labels-idx1-ubyte', num_img)

    nn = n.NeuralNet('a001_60000_4')


    tfunc = tf.Fp_funcs(int_bit, frac_bit)
    # Load nn and convert to fixed point
    nn.change_type(tfunc)
    # Convert test data to fixed point
    td_fp = c.change_data(td, tfunc)

    res = []
    # Start timing
    for i in imgs:
        pred = nn.feed_forward(td_fp[i][0])
        res = ftf.fp_res_to_float(pred)
        res.append((res, td_fp[i][1].to_float()))
        print(res)

        with open(f'fp_pred_again_img{i}', 'wb') as f:
            p.dump(res,f)

if(__name__ == '__main__'):
    run()


