import pickle as p
import numpy as np
import matplotlib.pyplot as plt

if (__name__ == '__main__'):
    p1 = []
    p2 = []
    p3 = []
    for i in range(3,21):
        with open(f'bootstrap{i}', 'rb') as f:
            tpl = p.load(f)
            p1.append(tpl[2])
            p2.append(tpl[3])
            p3.append(tpl[1]+tpl[4])


    psum = [p1[i]+p2[i]+p3[i] for i in range(0, len(p1))]
    p1 = [p1[i]/psum[i] for i in range(0, len(p1))]
    p2 = [p2[i]/psum[i] for i in range(0, len(p2))]
    p3 = [p3[i]/psum[i] for i in range(0, len(p3))]

    p23 = [p2[i]+p3[i] for i in range(0, len(p1))]

    #print(psum)


    N = len(p1)
    ind = np.arange(N)    # the x locations for the groups
    width = 0.35       # the width of the bars: can also be len(x) sequence

    b1 = plt.bar(ind, p1, width, bottom=p23)
    b2 = plt.bar(ind, p2, width, bottom=p3)
    b3 = plt.bar(ind, p3, width)


    plt.ylabel('Normalised split')
    plt.xlabel(r'$\lambda$')
    #plt.title('Scores by group and gender')
    plt.xticks(ind, range(3,3+N))
    plt.legend((b1[0], b2[0], b3[0]), ('P1', 'P2', 'P3'))

    plt.show()
