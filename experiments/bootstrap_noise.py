import math


def noise_bit(j, i):
    # base  
    if (j == 0):
        return 2*pi

    # recursive
    return 2*pi + noise_bit(j-1, i) + noise_carry(j, i)

def noise_carry(j, i):
    # base
    if (i == 0):
        return 0
    if (i == 1):
        return 2*pi + noise_bit(j, 0)

    # recursive
    res = (i-1)*4*pi
    for n in range(1, i):
        res += noise_bit(j, n) * 2
    res += noise_carry(j, 1)
    return res

def can_multiply(l,pi,alpha,beta):
    j, i = beta-2, math.ceil(math.log2(alpha)) + 1
    n_hhat_lsb = noise_carry(j, i) + 1
    n_bm = 2 * (n_hhat_lsb) + 1

    lhs = (2 ** n_bm)
    rhs = ((2 ** (l ** 2)) / 16)
    return (lhs < rhs)

if (__name__ == "__main__"):
    """
    print(noise_carry(0, 1)) # 28
    print(noise_carry(0, 2)) # 84
    print(noise_carry(0, 3)) # 140
    print(noise_carry(1, 1)) # 
    print(noise_carry(1, 2)) # 
    """

    l = 9
    pi = l

    beta, alpha = 2, 1

    print(can_multiply(l,pi,alpha,beta))
