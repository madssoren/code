import crypto.fhe as fh
import crypto.gentry_cipher as g
import pickle as p
import time


def make_timings(s):
    # fixture cipher params make it too slow
    a = 15
    b = 2185

    print(l)
    cipher = g.GentryCipher(l, a, b)

    (pk, sk) = cipher.keygen()
    fhe = fh.Fhe(cipher)
    sk_e = fhe.enc_list(pk, sk)
    c1 = cipher.enc(pk, 0)

    t1 = time.process_time()
    c3 = cipher.mul(pk, c1, c1)

    t2 = time.process_time()
    c3 = cipher.add(pk, c1, c1)

    t3 = time.process_time()
    t_add = t3-t2
    t_mul = t2-t1
    print(t_add)
    print(t_mul)

    return (f'l={l}', t_add, t_mul)


if (__name__ == '__main__'):
    ls = list(range(13,21))
    for l in ls:
        timing = make_timings(l)
        with open(f'add_mul{l}', 'wb') as f:
            p.dump(timing,f)


