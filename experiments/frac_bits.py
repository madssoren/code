import learning.nn as n
import learning.mnist as m
import time as t
import utility.change_img as c
import utility.type_funcs as tf
import pickle as p
import utility.fp_res_to_float as ftf
import sys
import multiprocessing as mp

def init():
    idxs = list(range(0,32))

    sizes = {0: [],1: [],2: [],3: []}


    for i in idxs[:16]:
        sizes[(i%4)].append(i)
        sizes[(i%4)].append(31-i)
    return sizes

    

def run(bit_sizes,pcs):
    int_bit = 12
    num_img = 30
    print('Number of fraction bits: '+str(bit_sizes))

    td = m.load_data('data/t10k-images-idx3-ubyte', 'data/t10k-labels-idx1-ubyte', num_img)

    nn = n.NeuralNet('a001_60000_4')

    res_i = []
    # Start timing
    for data in td:
        start = t.process_time()
        res = nn.feed_forward(data[0])
        stop = t.process_time()
        res_i.append((res,data[1],stop-start))
    with open('float_pred_pcs'+str(pcs), 'wb') as f:
            p.dump(res_i,f)

    for i in bit_sizes:
        tf_i = tf.Fp_funcs(int_bit, i)
        # Load nn and convert to fixed point
        nn = n.NeuralNet('a001_60000_4')
        nn.change_type(tf_i)
        # Convert test data to fixed point
        td_i = c.change_data(td, tf_i)

        res_i = []
        # Start timing
        for j,data in enumerate(td_i):
            start = t.process_time()
            print(f'Run with {i} frac bits, img {j}')
            res = ftf.fp_res_to_float(nn.feed_forward(data[0]))
            stop = t.process_time()
            res_i.append((res,data[1].to_float(), stop-start))
        with open('fp_pred_frac'+str(i), 'wb') as f:
                p.dump(res_i,f)

if(__name__ == '__main__'):
    sizes = init()
    processes = []
    for i in range(0,4):
        pr = mp.Process(target=run, args=(sizes[i],i))
        pr.start()
        processes.append(pr)

    for pr in processes:
        pr.join()
