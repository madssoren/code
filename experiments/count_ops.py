import math
import fractions as f

def func_nr(n_int, n_frac):
    r = f.Fraction(math.exp(1)) ** (f.Fraction(2 ** n_int))
    n = 1
    while ((2 ** n) < r):
        n += 1
    return n + n_frac

def f_addplus(n):
    return 8*n - 13

def f_addstar(n):
    return 4*n - 7

def f_mulplus(n):
    return 6*n + 1 + (n-1)*f_addplus(n)

def f_mulstar(n):
    return 3*(n-1) + (n ** 2) + (n-1)*f_addstar(n)

def f_expplus(n, n_int, n_frac):
    n_r = func_nr(n_int,n_frac)
    return (n-1)*f_mulplus(n_r) + 2 + 3*(n-1) + 2*(n-1)*(2*n_r)

def f_expstar(n, n_int, n_frac):
    n_r = func_nr(n_int,n_frac)
    return (n-1)*f_mulstar(n_r) + n_r + (n-1)*(2*n_r)

def f_maxplus(n):
    return 1

def f_maxstar(n):
    return n

def f_meanplus(n,k):
    return (k-1)*f_addplus(n) + f_mulplus(n)

def f_meanstar(n,k):
    return (k-1)*f_addstar(n) + f_mulstar(n)

def f_convplus(n,c,I,K,P):
    xi, yi = I
    xk, yk = K
    xp, yp = P
    a = xi - xk + 1
    b = yi - yk + 1
    return c*(xk*yk - 1)*(xi - xk)*(yi - yk)*f_addplus(n) + c*xk*yk*(xi - xk)*(yi - yk)*f_mulplus(n) + c*((math.ceil(a / xp))*(math.ceil(b / yp))*f_meanplus(n,xp*yp) + a*b*f_maxplus(n))

def f_convstar(n,c,I,K,P):
    xi, yi = I
    xk, yk = K
    xp, yp = P
    a = xi - xk + 1
    b = yi - yk + 1
    return c*(xk*yk - 1)*(xi - xk)*(yi - yk)*f_addstar(n) + c*xk*yk*(xi - xk)*(yi - yk)*f_mulstar(n) + c*((math.ceil(a / xp))*(math.ceil(b / yp))*f_meanstar(n,xp*yp) + a*b*f_maxstar(n))

def f_outputplus(n, N, s, n_int, n_frac):
    n_r = func_nr(n_int,n_frac)
    return s*(N*f_mulplus(n) + (N-1)*f_addplus(n) + f_expplus(n, n_int, n_frac)) + (s - 1)*f_addplus(n_r)

def f_outputstar(n, N, s, n_int, n_frac):
    n_r = func_nr(n_int,n_frac)
    return s*(N*f_mulstar(n) + (N-1)*f_addstar(n) + f_expstar(n, n_int, n_frac)) + (s - 1)*f_addstar(n_r)

def fprime_outputplus(n, N, s, n_int, n_frac):
    n_r = func_nr(n_int,n_frac)
    return s*(N*f_mulplus(n) + (N-1)*f_addplus(n))

def fprime_outputstar(n, N, s, n_int, n_frac):
    n_r = func_nr(n_int,n_frac)
    return s*(N*f_mulstar(n) + (N-1)*f_addstar(n))

if (__name__ == '__main__'):
    n_int = 12
    n_frac = 6
    n = n_int + n_frac
    c = 10
    I = (28,28)
    K = (9,9)
    P = (2,2)
    N = 10*(10)*c
    s = 10
    adds = f_convplus(n,c,I,K,P) + f_outputplus(n,N,s,n_int,n_frac)
    muls = f_convstar(n,c,I,K,P) + f_outputstar(n,N,s,n_int,n_frac)
    print(f'n_add = {adds}')
    print(f'n_mul = {muls}')

    adds = f_convplus(n,c,I,K,P) + fprime_outputplus(n,N,s,n_int,n_frac)
    muls = f_convstar(n,c,I,K,P) + fprime_outputstar(n,N,s,n_int,n_frac)
    print(f'n\'_add = {adds}')
    print(f'n\'_mul = {muls}')

    xi, yi = I
    xk, yk = K
    n_nomul = (n ** 2)*(s*N + c*xk*yk*(xi - xk + 1)*(yi - yk + 1))
    print(f'n_nomul = {n_nomul}')
