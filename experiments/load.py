import learning.mnist as m
import learning.nn as n
import random as r
import time as t

kern = [0.1*r.gauss(0.5, 1) for i in range(0,810)]
out = [r.gauss(0,0.01) for i in range(0,10000)]
rnd = kern+out
nn = n.NeuralNet((28,28), ((9,9), 10, (2,2)), 10, lambda : rnd.pop(0))

n = 10
train_data = m.load_data('data/train-images-idx3-ubyte', 'data/train-labels-idx1-ubyte', n)
test_data = m.load_data('data/t10k-images-idx3-ubyte', 'data/t10k-labels-idx1-ubyte', n)

nn.load('a001_60000_4')

def train(epochs = 1,alpha = 0.001):
    nn.train(train_data,epochs,alpha,'vars')

def pred():
    return nn.feed_forward(test_data[0][0], True)
