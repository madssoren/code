import crypto.fhe as fh
import crypto.gentry_cipher as g
import pickle as p
import time


def make_timings(l):
    # fixture cipher params make it too slow
    a = 15
    b = 2185

    print(l)
    cipher = g.GentryCipher(l, a, b)

    t1 = time.process_time()
    (pk, sk) = cipher.keygen()
    t2 = time.process_time()
    print(t2-t1)
    fhe = fh.Fhe(cipher)
    sk_e = fhe.enc_list(pk, sk)
    c1 = cipher.enc(pk, 0)

    print('begin recrypt timing')
    t = fhe.recrypt_timing(pk, sk_e, c1, c1)


    return (t2-t1,) + t


if (__name__ == '__main__'):
    ls = list(range(11,39))
    for l in ls:
        timing = make_timings(l)
        """
            file format:
            (keyGen, E(z), z*s, H = \sum h_i, 2xLSB+Round)
        """
        with open(f'bootstrap{l}', 'wb') as f:
            p.dump(timing,f)



