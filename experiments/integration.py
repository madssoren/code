import crypto.gentry_cipher as g
import crypto.fhe as e
import utility.type_funcs as tf
import utility.fp as f
import time


def test_multiply_n_bit_nums():
    num_int = 2
    num_frac = 0

    # Init crypto
    l = 7
    a = 1
    b = 2
    cipher = g.GentryCipher(l, a, b)

    (pk, sk) = cipher.keygen()
    fhe = e.Fhe(cipher)
    sk_e = fhe.enc_list(pk, sk)

    # Load
    type_func = tf.Cipher_funcs(num_int, num_frac, fhe, pk, sk_e, sk)
    num1 = type_func.init(1)
    num2 = type_func.init(1)
    start = time.process_time()
    res = num1 * num2
    stop = time.process_time()
    print(f'time: {stop-start}s')
    res_plain = f.fp(num_int, num_frac)
    res_plain.value = fhe.dec_list(sk, res.value)
    print(res_plain.to_float())
    assert res_plain.to_float() == 1


if (__name__ == '__main__'):
    test_multiply_n_bit_nums()
