import pickle as p
import math
import numpy as np
import matplotlib.pyplot as plt

def scales(lst):
    return [lst[i] / lst[i-1] for i in range(1,len(lst))]

def logs(lst):
    return [math.log2(x) for x in lst]

def bootstrap():
    p1 = []
    p2 = []
    p3 = []
    for i in range(3,21):
        with open(f'results/bootstrap{i}', 'rb') as f:
            tpl = p.load(f)
            p1.append(tpl[2])
            p2.append(tpl[3])
            p3.append(tpl[1]+tpl[4])

    psum = [p1[i]+p2[i]+p3[i] for i in range(0, len(p1))]
    s = scales(psum)
    s1 = scales(p1)
    s2 = scales(p2)
    s3 = scales(p3)

    s_avg = np.mean(s[-5:])
    #print(s)
    #print(s_avg)
    print(psum[-1] * (s_avg ** 52))

    #print(s1[-5:],s2[-5:], s3[-5:])
    #print(s1,s2, s3)
    plt.figure()
    plt.plot(s1)
    plt.plot(s2)
    plt.plot(s3)
    plt.figure()
    plt.plot(p1)
    plt.plot(p2)
    plt.plot(p3)

    plt.show()

def add_mul():
    p1 = []
    p2 = []
    p3 = []
    for i in range(3,21):
        with open(f'results/bootstrap{i}', 'rb') as f:
            tpl = p.load(f)
            p1.append(tpl[2])
            p2.append(tpl[3])
            p3.append(tpl[1]+tpl[4])

    psum = [p1[i]+p2[i]+p3[i] for i in range(0, len(p1))]
    #####
    adds = []
    muls = []
    for i in range(3, 21):
        with open(f'results/add_mul{i}', 'rb') as f:
            res = p.load(f)
            adds.append(res[1])
            muls.append(res[2])

    sa = scales(adds)
    la = logs(adds)
    sm = scales(muls)
    lm = logs(muls)

    a_avg = np.mean(sa[-2:])
    m_avg = np.mean(sm[-2:])

    #print(adds)
    #print(muls)
    plt.figure()
    ax = plt.subplot(1,1,1)
    ax.set_yscale('log')
    plt.plot(adds)
    plt.plot(muls)
    plt.plot(psum)
    #plt.plot(muls)
    plt.show()
    print('add: ', ((a_avg ** 52) * adds[-1]))
    print('mul: ', ((m_avg ** 52) * muls[-1]))


if (__name__ == '__main__'):
    bootstrap()
    add_mul()

