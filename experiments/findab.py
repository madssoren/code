import fractions as f
import math

def findab(alpha, lower, upper, th):
    if(upper-lower <= 1):
        if (choose(alpha, lower) >= th):
            return lower
        else:
            return upper

    beta_guess = upper - math.floor((upper-lower)/2)

    formula = choose(alpha, beta_guess)
    if (formula >= th):
        return findab(alpha, lower, beta_guess, th)
    else:   
        return findab(alpha, beta_guess, upper, th)


def choose(alpha, beta):
    res = beta
    for i in range(1, int(alpha)):
        res *= (beta-i)

    return res / f.Fraction(math.factorial(int(alpha)))

def findbeta(alpha):
    return int(findab(f.Fraction(alpha), f.Fraction(alpha), f.Fraction(2 ** 81), f.Fraction(2 ** 81)))
