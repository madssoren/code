import crypto.gentry_cipher as g
import math as m
import crypto.helpers as helpers
import utility.circuit as ci
import time

class Fhe:
    def __init__(self, cipher):
        self.cipher = cipher


    def enc_list(self, pk, l):
        res = []
        for e in l:
            res.append(self.cipher.enc(pk, e))
          
        return res

    def dec_list(self, sk, l):
        res = []
        for e in l:
            res.append(self.cipher.dec(sk, e))
          
        return res

    def recrypt(self, pk, sk_e, c):
        fn_add = lambda a,b : self.cipher.add_bit(pk, a, b) if b ==1 else self.cipher.add(pk, a, b)
        fn_mul = lambda a,b : self.cipher.mul(pk, a, b)
        c_old, z = c
        _, y = pk
        c_old_e = self.enc_list(pk, helpers.int_to_bin(c_old))
        z_e = []
        for e in z:
            e_temp_i, e_temp_d = helpers.get_decimal_bits(e, int(m.ceil(m.log2(self.cipher.a)))+1)
            z_e.append(self.enc_list(pk, e_temp_d+[e_temp_i]))

        H = []
        for i in range(0,self.cipher.b):
            t = []
            for e in z_e[i]:
                t.append(self.cipher.mul(pk, e, sk_e[i]))
            H.append(t)

        h_sum = H[0]
        for h in H[1:]:
            h_sum = ci.circuit_add(h_sum, h, fn_add, fn_mul)

        q_r = self.logic_round(pk, h_sum)
        return self.cipher.add(pk, self._lsb_ciphertext(c_old_e), q_r)

    def recrypt_timing(self, pk, sk_e, c, zero_enc):
        fn_add = lambda a,b : (a[0]+1, None) if b ==1 else (a[0]+b[0], None)
        fn_mul2 = lambda a,b : (a[0]*b[0], None) if type(b) != int else ((a[0], None) if b==1 else zero_enc)
        fn_mul = lambda a,b : fn_mul(b,a) if type(a) == int else fn_mul2(a,b)
        t1 = time.process_time()
        c_old, z = c
        _, y = pk
        c_old_e_lsb = self.cipher.enc(pk, (c_old & 1))
        z_e = []
        for e in z:
            e_temp_i, e_temp_d = helpers.get_decimal_bits(e, int(m.ceil(m.log2(self.cipher.a)))+1)
            z_e.append(e_temp_d+[e_temp_i])

        t2 = time.process_time()
        print(f'first: {t2-t1}')
        H = []
        for i in range(0,3):
            t = []
            for e in z_e[i]:
                t.append(fn_mul(e, sk_e[i]))
            H.append(t)

        t3 = time.process_time()
        print(f'cheat1: {t3-t2}')
        h_sum = H[0]
        for h in H[1:2]:
            #h_sum = self.logic_add(pk, h_sum, h)
            h_sum = ci.circuit_add(h_sum, h, fn_add, fn_mul)
        t4 = time.process_time()
        print(f'cheat2: {t4-t3}')

        q_r = self.logic_round(pk, h_sum)
        res = fn_add(c_old_e_lsb, q_r)

        t5 = time.process_time()
        print(f'last: {t5-t4}')
        return ((t2-t1), self.cipher.b*(t3-t2)/3, (len(H)-1)*(t4-t3)/2, (t5-t4))

    def _lsb_ciphertext(self, c):
        return c[0]

    def logic_round(self, pk, h):
        if (len(h) == 1):
            return h
        
        return self.cipher.add(pk, h[-2], h[-1])


    def logic_add(self, pk, h1, h2):
        fn_add = lambda a,b : self.cipher.add(pk, a, b)
        fn_mul = lambda a,b : self.cipher.mul(pk, a, b)

        return ci.circuit_add(h1, h2, fn_add, fn_mul)

    def logic_carry(self, pk, a, b, c):
        return self.logic_or(pk, self.logic_and(pk, a, b), self.logic_and(pk, self.logic_or(pk, a, b), c))

    def logic_and(self, pk, c1, c2):
        return self.cipher.mul(pk, c1, c2)

    def logic_or(self, pk, c1, c2):
        return self.cipher.add_bit(pk, self.cipher.mul(pk, self.cipher.add_bit(pk, c1, 1), self.cipher.add_bit(pk, c2, 1)), 1)

    def cipher_add3(self, pk, a, b, c):
        temp = self.cipher.add(pk, a, b)
        return self.cipher.add(pk, temp, c)
