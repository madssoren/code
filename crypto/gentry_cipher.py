import random as r
import decimal as d
import math
import crypto.sssp as s
import fractions as f

def random_nbit(n):
    return r.randint(0,2**n) | 2**n

class GentryCipher:
    def __init__(self, l, alpha, beta):
        self.N = l
        self.P = l ** 2
        self.Q = l ** 5
        self.a = alpha
        self.b = beta

    def _keygen(self):
        sk = 2
        while (sk % 2 == 0):
            sk = random_nbit(self.P)

        #n = self.N*2
        n = 1410
        pk = [self._enc(sk,0) for i in range(0, n) ]

        return (pk, sk)


    def keygen(self):
        (pk, sk) = self._keygen()
        self.p = sk # ONLY for testing purposes (Exposes SECRET to everyone!)
        y = [f.Fraction(r.random()*2) for i in range(0,self.b)]
        pk_new = (pk, y)

        alpha_idx = set()
        while (len(alpha_idx) != self.a-1):
            idx = r.randint(0, self.b-1)
            alpha_idx.add(idx)

        inv = (0-sum([y[i] for i in alpha_idx])) % 2

        while (True):
            idx = r.randint(0, self.b-1)
            if idx not in alpha_idx:
                y[idx] = inv + f.Fraction(1, sk)
                alpha_idx.add(idx)
                break
        
        sk_new = [0]*self.b
        for i in alpha_idx:
            sk_new[i] = 1

        return (pk_new, sk_new)


    def _enc(self, p, m):
        mp = m+1
        while(mp % 2 != m):
            mp = random_nbit(self.N)
        q = random_nbit(self.Q)
        return mp + p*q


    def _enc2(self, pk, m):
        c_temp = pk[r.randint(0, len(pk)-1)]
        """ old impl
        n = r.randint(0, 2**len(pk))
        n = 0

        while(n != 0):
            c_temp += pk[n & 1]
            n = n >> 1
        """
        for i in range(0,14):
            c_temp += pk[r.randint(0, len(pk)-1)]
        return m + c_temp

    def enc(self, pk, m):
        pk_old, y = pk
        c = self._enc2(pk_old, m)

        z = self.make_z(c, y)
        return (c, z)

    def make_z(self, c, y):
        return [round(float((c*f.Fraction(e))%2), math.ceil(math.log10(self.a))) for e in y]

            
    def dec(self, sk, c):
        c_old, z = c
        s = sum([z[i]*sk[i] for i in range(0, self.b)])
        s = round(s) % 2
            

        return (1 & c_old) ^ (1 & s)

    def add(self, pk, c1, c2):
        c1_old, _ = c1
        c2_old, _ = c2
        _, y = pk

        c_new = c1_old + c2_old
        z_new = self.make_z(c_new, y)

        return (c_new, z_new)


    def mul(self, pk, c1, c2):
        c1_old, _ = c1
        c2_old, _ = c2
        _, y = pk

        c_new = c1_old * c2_old
        z_new = self.make_z(c_new, y)

        return (c_new, z_new)

    def add_bit(self, pk, c, b):
        _, y = pk
        c_old, z = c
        c_new = c_old + b
        z_new = self.make_z(c_new, y)
        return (c_new, z_new)
