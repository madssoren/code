def get_decimal_bits(fl, num_decimals_bits):
    i = int(fl)
    ds = []

    d = fl-i
    d = int(d * 2**num_decimals_bits)

    for j in range(0,num_decimals_bits):
        ds.append(1 & d)
        d = d >> 1
    
    return (i, ds)

def lsb(num):
    return 1 & int(num)

def int_to_bin(num):
    b = []
    while (num != 0):
        b.append(1 & num)
        num = num >> 1

    return b
