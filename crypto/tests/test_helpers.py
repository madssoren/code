import crypto.helpers as h

def test_float_to_bin1():
    n = 0.754
    num_digit = 4

    i, d = h.get_decimal_bits(n, num_digit)
    d_expected = [1,1,0,0]
    d_expected.reverse()
    assert i == 0
    assert d == d_expected

def test_float_to_bin2():
    n = 0.1
    num_digit = 8

    i, d = h.get_decimal_bits(n, num_digit)
    d_expected = [0,0,0,1,1,0,0,1]
    d_expected.reverse()
    assert i == 0
    assert d == d_expected

def test_float_to_bin3():
    n = 0.07
    num_digit = 6

    i, d = h.get_decimal_bits(n, num_digit)
    d_expected = [0,0,0,1,0,0]
    d_expected.reverse()
    assert i == 0
    assert d == d_expected

def test_float_to_bin4():
    n = 0.0
    num_digit = 6

    i, d = h.get_decimal_bits(n, num_digit)
    assert i == 0
    assert d == [0,0,0,0,0,0]

def test_float_to_bin5():
    n = 1.999
    num_digit = 6

    i, d = h.get_decimal_bits(n, num_digit)
    assert i == 1
    assert d == [1,1,1,1,1,1]

def test_int_to_bin1():
    n = 17
    assert h.int_to_bin(n) == [1,0,0,0,1]

def test_int_to_bin2():
    n = 53
    bin_expected = [1,1,0,1,0,1]
    bin_expected.reverse()
    assert h.int_to_bin(n) == bin_expected
