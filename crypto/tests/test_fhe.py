import pytest
import crypto.fhe as f
import crypto.gentry_cipher as g
import crypto.helpers as h

@pytest.fixture
def cipher():
    l = 11
    a = 1
    b = 2
    return g.GentryCipher(l, a, b)

def test_reencrypt():
    # fixture cipher params make it too slow
    l = 7
    a = 1
    b = 2
    cipher = g.GentryCipher(l, a, b)

    (pk, sk) = cipher.keygen()
    m=1
    c = cipher.enc(pk, m)
    fhe = f.Fhe(cipher)
    sk_e = fhe.enc_list(pk, sk)
    c_new = fhe.recrypt(pk, sk_e, c)

    assert cipher.dec(sk, c_new) == m

"""
def test_reencrypt_and_mul():
    # fixture cipher params make it too slow
    l = 9
    a = 1
    b = 2
    cipher = g.GentryCipher(l, a, b)

    (pk, sk) = cipher.keygen()
    c1 = cipher.enc(pk, 0)
    c2 = cipher.enc(pk, 1)
    fhe = f.Fhe(cipher)
    sk_e = fhe.enc_list(pk, sk)
    c1_new = fhe.recrypt(pk, sk_e, c1)
    c2_new = fhe.recrypt(pk, sk_e, c2)
    c3 = cipher.mul(pk, c1_new, c2_new)
    #c_new = fhe.recrypt(pk, sk_e, c3)

    assert cipher.dec(sk, c3) == 0
"""

def test_logic_or(cipher):
    (pk, sk) = cipher.keygen()
    c0 = cipher.enc(pk, 0)
    c1 = cipher.enc(pk, 1)

    fhe = f.Fhe(cipher)
    assert cipher.dec(sk, fhe.logic_or(pk, c0, c0)) == 0
    assert cipher.dec(sk, fhe.logic_or(pk, c0, c1)) == 1
    assert cipher.dec(sk, fhe.logic_or(pk, c1, c0)) == 1
    assert cipher.dec(sk, fhe.logic_or(pk, c1, c1)) == 1
    
def test_logic_and(cipher):
    (pk, sk) = cipher.keygen()
    c0 = cipher.enc(pk, 0)
    c1 = cipher.enc(pk, 1)

    fhe = f.Fhe(cipher)
    assert cipher.dec(sk, fhe.logic_and(pk, c0, c0)) == 0
    assert cipher.dec(sk, fhe.logic_and(pk, c0, c1)) == 0
    assert cipher.dec(sk, fhe.logic_and(pk, c1, c0)) == 0
    assert cipher.dec(sk, fhe.logic_and(pk, c1, c1)) == 1

def test_logic_carry(cipher):
    (pk, sk) = cipher.keygen()
    c0 = cipher.enc(pk, 0)
    c1 = cipher.enc(pk, 1)

    fhe = f.Fhe(cipher)
    assert cipher.dec(sk, fhe.logic_carry(pk, c0, c0, c0)) == 0
    assert cipher.dec(sk, fhe.logic_carry(pk, c0, c0, c1)) == 0
    assert cipher.dec(sk, fhe.logic_carry(pk, c0, c1, c0)) == 0
    assert cipher.dec(sk, fhe.logic_carry(pk, c0, c1, c1)) == 1
    assert cipher.dec(sk, fhe.logic_carry(pk, c1, c0, c0)) == 0
    assert cipher.dec(sk, fhe.logic_carry(pk, c1, c0, c1)) == 1
    assert cipher.dec(sk, fhe.logic_carry(pk, c1, c1, c0)) == 1
    assert cipher.dec(sk, fhe.logic_carry(pk, c1, c1, c1)) == 1

def test_logic_add3(cipher):
    (pk, sk) = cipher.keygen()
    c0 = cipher.enc(pk, 0)
    c1 = cipher.enc(pk, 1)

    fhe = f.Fhe(cipher)
    assert cipher.dec(sk, fhe.cipher_add3(pk, c0, c0, c0)) == 0
    assert cipher.dec(sk, fhe.cipher_add3(pk, c0, c0, c1)) == 1
    assert cipher.dec(sk, fhe.cipher_add3(pk, c0, c1, c0)) == 1
    assert cipher.dec(sk, fhe.cipher_add3(pk, c0, c1, c1)) == 0
    assert cipher.dec(sk, fhe.cipher_add3(pk, c1, c0, c0)) == 1
    assert cipher.dec(sk, fhe.cipher_add3(pk, c1, c0, c1)) == 0
    assert cipher.dec(sk, fhe.cipher_add3(pk, c1, c1, c0)) == 0
    assert cipher.dec(sk, fhe.cipher_add3(pk, c1, c1, c1)) == 1
