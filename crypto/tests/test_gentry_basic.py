import pytest
import crypto.gentry_basic as g
import math as m

@pytest.fixture
def cipher():
    l = 5
    return g.GentryBasic(l)
    

def test_can_decrypt(cipher):
    (pk, sk) = cipher.keygen()
    assert cipher.dec(sk, cipher.enc(pk, 0)) == 0
    assert cipher.dec(sk, cipher.enc(pk, 1)) == 1

def test_key_len(cipher):
    (pk, sk) = cipher.keygen()
    assert cipher.P == int(m.ceil(m.log2(sk)))
    
def test_homomorphism_addition(cipher):
    (pk, sk) = cipher.keygen()
    m0, m1 = 0, 1

    c0 = cipher.enc(pk, m0)
    c1 = cipher.enc(pk, m1)

    assert (m0+m0)%2 == cipher.dec(sk, c0+c0)
    assert (m0+m1)%2 == cipher.dec(sk, c0+c1)
    assert (m1+m1)%2 == cipher.dec(sk, c1+c1)
    
def test_homomorphism_multication(cipher):
    (pk, sk) = cipher.keygen()
    m0, m1 = 0, 1

    c0 = cipher.enc(pk, m0)
    c1 = cipher.enc(pk, m1)

    assert (m0*m0)%2 == cipher.dec(sk, c0*c0)
    assert (m0*m1)%2 == cipher.dec(sk, c0*c1)
    assert (m1*m1)%2 == cipher.dec(sk, c1*c1)
