import pytest
import crypto.gentry_cipher as g
import math as m
import fractions as f

@pytest.fixture
def cipher():
    l = 9
    a = 3
    b = 10
    return g.GentryCipher(l, a, b)
    

def test_keygen(cipher):
    (pk, sk) = cipher.keygen()
    _,y = pk
    s = sum([f.Fraction(y[i])*sk[i] for i in range(0, cipher.b)])
    s = s % 2
    assert round(1/s, 0) == cipher.p

def test_encrypt(cipher):
    (pk, sk) = cipher.keygen()
    m = 1
    c = cipher.enc(pk, m)
    cc, z = c
    print (z[0])

    s = sum([z[i]*sk[i] for i in range(0, cipher.b)])
    assert round(s) % 2 == round(f.Fraction(cc, cipher.p)) % 2

def test_can_decrypt(cipher):
    (pk, sk) = cipher.keygen()
    assert cipher.dec(sk, cipher.enc(pk, 0)) == 0
    assert cipher.dec(sk, cipher.enc(pk, 1)) == 1

def test_homomorphism_addition(cipher):
    (pk, sk) = cipher.keygen()
    m0, m1 = 0, 1

    c0 = cipher.enc(pk, m0)
    c1 = cipher.enc(pk, m1)

    assert (m0+m0)%2 == cipher.dec(sk, cipher.add(pk, c0, c0))
    assert (m0+m1)%2 == cipher.dec(sk, cipher.add(pk, c0, c1))
    assert (m1+m1)%2 == cipher.dec(sk, cipher.add(pk, c1, c1))
    
def test_homomorphism_multication(cipher):
    (pk, sk) = cipher.keygen()
    m0, m1 = 0, 1

    c0 = cipher.enc(pk, m0)
    c1 = cipher.enc(pk, m1)

    assert (m0*m0)%2 == cipher.dec(sk, cipher.mul(pk, c0, c0))
    assert (m0*m1)%2 == cipher.dec(sk, cipher.mul(pk, c0, c1))
    assert (m1*m1)%2 == cipher.dec(sk, cipher.mul(pk, c1, c1))

def test_add_bit(cipher):
    (pk, sk) = cipher.keygen()
    m0, m1 = 0, 1

    c0 = cipher.enc(pk, m0)
    c1 = cipher.enc(pk, m1)

    assert (m0+0)%2 == cipher.dec(sk, cipher.add_bit(pk, c0, 0))
    assert (m1+0)%2 == cipher.dec(sk, cipher.add_bit(pk, c1, 0))
    assert (m0+1)%2 == cipher.dec(sk, cipher.add_bit(pk, c0, 1))
    assert (m1+1)%2 == cipher.dec(sk, cipher.add_bit(pk, c1, 1))
