import crypto.sssp as s

def test_perm_rec():
    a = []
    s.perm_rec([1,2,3,4,5], [], 3, a)
    assert a == [[1, 2, 3], [1, 2, 4], [1, 2, 5], [1, 3, 4], [1, 3, 5], [1, 4, 5], [2, 3, 4], [2, 3, 5], [2, 4, 5], [3, 4, 5]]

def test_sssp():
    pass
