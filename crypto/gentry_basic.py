import random as r

l = 3

N = l
P = l ** 2
Q = l ** 5
def random_nbit(n):
    return r.randint(0,2**(n-1)) | 2**(n-1)

class GentryBasic:
    def __init__(self, l):
        self.N = l
        self.P = l ** 2
        self.Q = l ** 5

    def keygen(self):
        sk = 2
        while (sk % 2 == 0):
            sk = random_nbit(self.P)

        n = self.N*2
        pk = [self._enc(sk,0) for i in range(0, n) ]

        return (pk, sk)

    def _enc(self, p, m):
        mp = m+1
        while(mp % 2 != m):
            mp = random_nbit(self.N)
        q = random_nbit(self.Q)
        return mp + p*q

    def enc(self, pk, m):
        c_temp = pk[r.randint(0, len(pk)-1)]
        n = r.randint(0, 2**len(pk))

        while(n != 0):
            c_temp += pk[n & 1]
            n = n >> 1
        
        return m + c_temp
            
    def dec(self, p, c):
        return (c % p) % 2

