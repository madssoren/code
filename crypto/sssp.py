

def sssp(elems, res, num_elems, precision):
    perms = []
    perm_rec([i for i in range(0,len(elems))], [], num_elems, perms)

    best_perm = []
    best_error = 2
    for perm in perms:
        score = sum([elems[i] for i in perm]) % 2
        score = score-res
        if (abs(score) < best_error):
            best_error = score
            best_perm = perm

    s = [0]*len(elems)
    for i in best_perm:
        s[i] = 1

    print (f'best score={best_error}')
    print (f'1/p={res}')
    return s

        
    
    
        

def perm_rec(remaining_elems, chosen_elems, lvl, res):
    if (lvl == 0):
        res.append(chosen_elems)
    
    for e in remaining_elems.copy():
        new_chosen = chosen_elems.copy()
        new_chosen.append(e)

        new_remaining = remaining_elems.copy()
        new_remaining.remove(e)

        perm_rec(new_remaining, new_chosen, lvl-1, res)

        remaining_elems.remove(e)


