import pickle as p
import matplotlib.pyplot as plt

plt.rc('font', size=10, family='serif')
plt.rc('axes', labelsize=12)
plt.rc('mathtext', fontset='cm')
plt.rc('figure', figsize=(3.0,3.0))

fp_res = []
for i in range(0,32):
    with open('../binaryfiles/results/fp_pred_frac'+str(i), 'rb') as f:
        fp_res.append(p.load(f))
        
float_res = []
with open('../binaryfiles/results/float_pred_pcs0','rb') as f:
    float_res = p.load(f)

float_confs = []
float_preds = []
float_time = 0
float_rate = 0

for img in float_res:
    confs = []
    for frac in img[0]:
        confs.append(float(frac[0]/frac[1]))
    float_confs.append(confs)
    pred = confs.index(max(confs))
    float_preds.append(pred)
    float_rate += (pred == img[1])/30
    float_time += img[2]/30

fp_confs = []
fp_preds = []
fp_times = []
fp_rates = []

for bit_size in fp_res:
    bit_size_confs = []
    bit_size_preds = []
    bit_size_rate = 0
    bit_size_time = 0
    for img in bit_size:
        confs = []
        for frac in img[0]:
            confs.append(frac[0]/frac[1])
        bit_size_confs.append(confs)
        pred = confs.index(max(confs))
        bit_size_preds.append(pred)
        bit_size_rate += (pred == img[1])/30
        bit_size_time += img[2]/30

    fp_confs.append(bit_size_confs)
    fp_preds.append(bit_size_preds)
    fp_times.append(bit_size_time)
    fp_rates.append(bit_size_rate)

percent_time = []
for i in fp_times:
    percent_time.append((i/float_time))
comp_preds = []
for bit_size in fp_preds:
    comp = 0
    for i in range(0,30):
        comp += (bit_size[i] == float_preds[i])/30
    comp_preds.append(comp)

comp_pred_diffs = []
for j in range(1,len(comp_preds)):
    diff = comp_preds[j] - comp_preds[j-1]
    comp_pred_diffs.append(diff)
    

square_err = []
for bit_size in fp_confs:
    err = 0
    for i in range(0,30):
        for j in range(0,10):
            err += (float_confs[i][j]-bit_size[i][j])**2
    square_err.append(err/30)    

""" Plotting """
plt.figure()
plt.scatter(range(0,32),percent_time,marker='o',color='steelblue', facecolors='None',linestyle='None')
plt.ylabel(r'$t_{fp}/t_c$')
plt.xlabel('Number of fraction bits')
plt.savefig('pred_time', format='eps', bbox_inches='tight', pad_inches=0.15)
print(percent_time)

plt.figure()
plt.scatter(range(0,32),comp_preds,marker='o',color='steelblue', facecolors='None',linestyle='None')
plt.ylabel('Prediction rate')
plt.xlabel('Number of fraction bits')
plt.savefig('pred_rate', format='eps', bbox_inches='tight', pad_inches=0.15)
print(comp_preds)

plt.figure()
plt.scatter(range(0,31),comp_pred_diffs,marker='o',color='steelblue', facecolors='None')
plt.ylabel('Prediction rate')
plt.xlabel('Number of fraction bits')
plt.savefig('pred_rate_diff', format='eps', bbox_inches='tight', pad_inches=0.15)

plt.figure()
plt.scatter(range(0,32),square_err,marker='o',color='steelblue', facecolors='None',linestyle='None')
plt.ylabel('ASSE')
plt.xlabel('Number of fraction bits')
plt.savefig('squared_err', format='eps', bbox_inches='tight', pad_inches=0.15)
print(square_err) 
