import matplotlib.pyplot as plt

x_values = list(range(0,100))
y_sigmoid = list(range(0,100))
y_tanh = list(range(0,100))
y_relu = list(range(0,100))

plt.figure(1)

plt.subplot(131)
plt.plot(x_values,y_sigmoid)

plt.subplot(132)
plt.plot(x_values,y_tanh)

plt.subplot(133)
plt.plot(x_values,y_relu)

plt.show()
