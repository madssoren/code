import matplotlib.pyplot as plt
import learning.mnist as mn

plt.rc('font', size=10, family='serif')
plt.rc('axes', labelsize=12)
plt.rc('mathtext', fontset='cm')
plt.rc('figure', figsize=(6.0,3.0))

if __name__ == '__main__':
    imgs = mn.load_images('data/t10k-images-idx3-ubyte',8)

    plt.figure()
    plt.gray()
    for i in range(0,len(imgs)):
        plt.subplot(241+i)
        plt.imshow(imgs[i])   
        plt.axis('off')
    plt.savefig('mnisteight', format='eps', bbox_inches='tight', pad_inches=0.15)

    plt.figure()
    plt.rc('figure', figsize=(3.0,3.0))
    plt.imshow(imgs[1])
    plt.axis('off')
    plt.savefig('mnistsample.png', bbox_inches='tight')
