import matplotlib.pyplot as plt
import math as m
import numpy as np

plt.rc('font', size=10, family='serif')
plt.rc('axes', labelsize=12)
plt.rc('mathtext', fontset='cm')
plt.rc('figure', figsize=(3.0,3.0))


x = list(range(3,20))
y = [0.10900797433413345, 0.08585770366606008, 0.09863112099960367, 0.12394299034110645, 0.2030628200409789, 0.29052526797742456, 0.4766012432914219, 0.84209463536907, 1.437260129563962, 2.3674698409719594, 3.719106401971051, 6.163359215971164, 9.53135509436106, 15.00805177475316, 22.101920291907543, 33.75451980168873, 47.86395350903331]


plt.figure()
ax = plt.subplot(1,1,1)
ax.set_yscale('log')
plt.plot(x,y)
plt.show()

scale = [y[i] / y[i-1] for i in range(1,17)]
print(np.mean(scale))


