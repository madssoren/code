def fp_res_to_float(res):
    res_float = []
    for fps in res:
        res_float.append((fps[0].to_float(),fps[1].to_float()))
    return res_float
