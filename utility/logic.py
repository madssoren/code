def logic_add3(a, b, c, fn_add):
    return fn_add(a, fn_add(b, c))

def logic_and(c1, c2, fn_mul):
    return fn_mul(c1, c2)

def logic_or(c1, c2, fn_add, fn_mul):
    return fn_add(fn_mul(fn_add(c1, 1), fn_add(c2, 1)), 1)

