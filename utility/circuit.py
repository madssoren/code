import utility.logic as l


def circuit_carry(a, b, c, fn_add, fn_mul):
    return l.logic_or(l.logic_and(a, b, fn_mul), l.logic_and(l.logic_or(a, b, fn_add, fn_mul), c, fn_mul), fn_add, fn_mul)

def circuit_add(v1, v2, fn_add, fn_mul):
    v_res = []
    v_res.append(fn_add(v1[0], v2[0]))

    if (len(v1) == 1):
        return v_res

    c1 = fn_mul(v1[0], v2[0])
    v_res.append(l.logic_add3(v1[1], v2[1], c1, fn_add))

    a = v1[1]
    b = v2[1]
    c = c1
    for i in range(2, len(v1)):
        c = circuit_carry(a, b, c, fn_add, fn_mul)
        a = v1[i]
        b = v2[i]

        v_res.append(l.logic_add3(a, b, c, fn_add))

    return v_res

def circuit_mean(fps):
    pass
