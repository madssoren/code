import statistics as s
import math as m
import fractions as fr
import utility.fp as f
import utility.exp_table as t
import utility.circuit_exp as ce

class Type_funcs:
    def init(self, x):
        pass

    def mean(self, x):
        pass

    def max_zero(self, x):
        pass

    def exp(self, x):
        pass

class Float_funcs(Type_funcs):
    def init(self, x):
        return x

    def mean(self, x):
        return s.mean(x)

    def max_zero(self, x):
        return max([0, x])

    def exp(self, x):
        x_int = int(x)
        x_fraction = x - x_int
        e = fr.Fraction(m.exp(1))
        return fr.Fraction(e ** x_int)*fr.Fraction(e ** x_fraction)
        return m.exp(x)

class Fp_funcs(Type_funcs):
    def __init__(self, num_int_bits, num_float_bits):
        self.num_int_bits = num_int_bits
        self.num_float_bits = num_float_bits
        self.table = t.exp_indices(num_int_bits, num_float_bits, m.exp(1))

    def init(self, x):
        return f.fp(self.num_int_bits, self.num_float_bits, x)

    def mean(self, items):
        res = items[0]

        for item in items[1:]:
            res += item

        mul_inverse = self.init(1/len(items))
        return res * mul_inverse

    def max_zero(self, x):
        sign = x.value[-1]
        inv_sign = x._fn_add(1, sign)

        new_val = []
        for v in x.value:
            new_val.append(x._fn_mul(inv_sign, v))
            
        new_fp = f.fp(x.num_int_bits, x.num_float_bits)
        new_fp.value = new_val
        return new_fp

    def exp(self, x):
        return ce.circuit_exp(x, self.table, x._fn_add, x._fn_mul)

class Cipher_funcs(Fp_funcs):
    def __init__(self, num_int_bits, num_float_bits, fhe, pk, sk_e, sk = None):
        self.num_int_bits = num_int_bits
        self.num_float_bits = num_float_bits
        self.fhe = fhe
        self.pk = pk
        self.sk_e = sk_e
        self.table = t.exp_indices(num_int_bits, num_float_bits, m.exp(1))
        self.sk = sk

    def _add(self, a, b):
        #print('start add')
        if (type(b) == int):
            b_old = b
            b = a
            a = b_old

        if (type(a) == int):
            c = self.fhe.cipher.add_bit(self.pk, b, a)
            a_pr = a
        else:
            c = self.fhe.cipher.add(self.pk, a, b)
            if (self.sk != None):
                a_pr = self.fhe.cipher.dec(self.sk, a)

        res = self.fhe.recrypt(self.pk, self.sk_e, c)
        if (self.sk != None):
            print(f'{a_pr} + {self.fhe.cipher.dec(self.sk, b)} = {self.fhe.cipher.dec(self.sk, res)}')
        #print('stop add')
        return res
        
    def _mul(self, a, b):
        #print('start mul')
        c = self.fhe.cipher.mul(self.pk, a, b)
        res = self.fhe.recrypt(self.pk, self.sk_e, c)
        if (self.sk != None):
            print(f'{self.fhe.cipher.dec(self.sk, a)} * {self.fhe.cipher.dec(self.sk, b)} = {self.fhe.cipher.dec(self.sk, res)}')
        #print('stop mul')
        return res

    def init(self, x):
        fp = f.fp(self.num_int_bits, self.num_float_bits, x)
        fp._fn_add = self._add
        fp._fn_mul = self._mul
        for i in range(0, fp.len):
            fp.value[i] = self.fhe.cipher.enc(self.pk, fp.value[i])
        return fp
    
