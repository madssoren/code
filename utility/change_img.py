import utility.type_funcs as tf

def change_data(data_tuples, type_func):
    new_tuples = []
    for data in data_tuples:
        new_img = [[type_func.init(p) for p in row] for row in data[0]]
        new_tuples.append((new_img, type_func.init(data[1])))

    return new_tuples
    
