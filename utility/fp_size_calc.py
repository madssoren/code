import learning.nn as n
import functools as f
import operator as o
import math as m

def fp_size(nn):
    kern_min = 0
    kern_max = 0
    out_min = 0
    out_max = 0

    # theta kernel
    for i in range(0, nn.channels):
        theta_kerns = f.reduce(o.concat, nn.theta_kernels[i])
        min_sum, max_sum = sum_min_max(theta_kerns)

        kern_max = max(max_sum, kern_max)
        kern_min = min(min_sum, kern_min)
            
   # theta output 
    for i in range(0, nn.output):
        min_sum, max_sum = sum_min_max(nn.theta_output[i])

        out_max = max(max_sum*kern_max, out_max)
        out_min = min(min_sum*kern_max, out_min)
        
    max_val = max(out_max, kern_max, abs(out_min), abs(kern_min))

    return m.ceil(m.log2(int(max_val)))
        

def sum_min_max(values):
    min_sum = 0
    max_sum = 0

    for val in values:
        if (val < 0):
            min_sum += val
        else:
            max_sum += val

    return (min_sum, max_sum)
    

