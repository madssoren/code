import utility.circuit as c
import learning.helper as h

class fp:
    def __init__(self, num_int_bits, num_float_bits, value = 0):
        self.len = num_int_bits + num_float_bits
        self.value = [0]*self.len
        self.num_int_bits = num_int_bits
        self.num_float_bits = num_float_bits
        self._fn_add = lambda x, y : (x+y) % 2
        self._fn_mul = lambda x, y : (x*y) % 2
        self._set_value(value)

    def _twos_compliment(self, value, bit = 1):
        count = len(value)
        new_value = [0] * self.len
        for i in range(0, count):
            new_value[i] = self._fn_add(bit, value[i])

        carry = bit
        for idx in range(0, self.len):
            new_carry = self._fn_mul(new_value[idx], carry)
            new_value[idx] = self._fn_add(carry, new_value[idx])
            carry  = new_carry

        return new_value

    def _set_value(self, value):
        sign = 0 if value >= 0 else 1
        value = abs(value)
        integer = int(value)

        floating = value - integer
        floating *= 2 ** self.num_float_bits
        floating = int(floating)

        for i in range(0, self.num_float_bits):
            self.value[i] = 1 & floating
            floating = floating >> 1

        for i in range(self.num_float_bits, self.len):
            self.value[i] = 1 & integer
            integer = integer >> 1

        self.value = self._twos_compliment(self.value, sign)

    def to_float(self):
        result = 0.0
        sign = self.value[-1]
        value = []
        if (sign == 1):
            value = self._twos_compliment(self.value)
        else:
            value = self.value

        for i in range(0, self.num_float_bits):
            result += ((2 ** i) / (2 ** (self.len-self.num_int_bits))) if value[i] == 1 else 0


        for i in range(self.num_float_bits, self.len):
            result += (2 ** (i - self.num_float_bits)) if value[i] == 1 else 0

        return result if sign == 0 else 0 - result

    def to_int(self):
        result = 0
        sign = self.value[-1]
        value = []
        if (sign == 1):
            value = self._twos_compliment(self.value)
        else:
            value = self.value

        for i in range(self.num_float_bits, self.len):
            result += (2 ** (i - self.num_float_bits)) if value[i] == 1 else 0

        return result if sign == 0 else 0 - result

    def __add__(self, other):
        assert self.num_int_bits == other.num_int_bits
        assert self.len == other.len
        
        new_fp = fp(self.num_int_bits, self.num_float_bits)
        new_fp.value = c.circuit_add(self.value, other.value, self._fn_add, self._fn_mul)
        new_fp._fn_add, new_fp._fn_mul = self._fn_add, self._fn_mul
        return new_fp

    def _mul_old(self, other):
        assert self.num_int_bits == other.num_int_bits
        assert self.len == other.len

        products = h.build_multi_dim([self.len*2, self.len*2])
        shift = 0
        for i in range(0, self.len*2):
            for j in range(0, self.len*2):
                if (j+shift >= self.len*2):
                    break
                products[i][j+shift] = self._fn_mul(self.value[min(i, self.len-1)], other.value[min(j, self.len-1)])
            shift += 1


        #for p in products:
        #    print(p)
        res = products[0]
        for i in range(1, self.len*2):
            res = c.circuit_add(res, products[i], self._fn_add, self._fn_mul)
        #print(res)
        #res = res[0 : self.len*2]
            

        new_fp = fp(self.num_int_bits, self.num_float_bits)
        start_idx = (self.len-self.num_int_bits)
        new_fp.value = res[start_idx : start_idx + self.len]
        new_fp._fn_add, new_fp._fn_mul = self._fn_add, self._fn_mul
        return new_fp

    def __mul__(self, other):
        assert self.num_int_bits == other.num_int_bits
        assert self.len == other.len

        # two's comp with sign (conv to pos)
        self_sign = self.value[-1]
        other_sign = other.value[-1]
        #print('start complements')
        self_value = self._twos_compliment(self.value, self_sign)
        other_value = other._twos_compliment(other.value, other_sign)

        res_num_bits = 2*self.len - self.num_int_bits 
        products = h.build_multi_dim([self.len, res_num_bits])
        shift = 0

        #print('start build prod table')
        for i in range(0, self.len):
            for j in range(0, self.len):
                if (j+shift >= res_num_bits):
                    break
                products[i][j+shift] = self._fn_mul(self_value[i], other_value[j])
            shift += 1

        res = products[0]
        #print('start intermediary additions')
        for i in range(1, self.len):
            res[i:] = c.circuit_add(res[i:], products[i][i:], self._fn_add, self._fn_mul)

        new_fp = fp(self.num_int_bits, self.num_float_bits)
        start_idx = (self.len-self.num_int_bits)
        new_fp.value = res[start_idx : start_idx + self.len]
        new_fp._fn_add, new_fp._fn_mul = self._fn_add, self._fn_mul

        new_sign = self._fn_add(self_sign, other_sign)
        new_fp.value = self._twos_compliment(new_fp.value, new_sign)
        return new_fp
