import utility.fp as f
import pytest

def test_integer():
    assert f.fp(5, 5, 7).value == [0, 0, 0, 0, 0, 1, 1, 1, 0, 0]
    assert f.fp(5, 5, -7).value == [0, 0, 0, 0, 0, 1, 0, 0, 1, 1]
    assert f.fp(5, 5, 0).value == [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

def test_float():
    assert f.fp(5, 5, 7.75).value == [0, 0, 0, 1, 1, 1, 1, 1, 0, 0]
    assert f.fp(5, 5, 7.6).value == [1, 1, 0, 0, 1, 1, 1, 1, 0, 0]
    assert f.fp(5, 5, -7.75).value == [0, 0, 0, 1, 0, 0, 0, 0, 1, 1]
    assert f.fp(5, 5, -7.6).value == [1, 0, 1, 1, 0, 0, 0, 0, 1, 1]
    assert f.fp(5, 5, -7.0).value == [0, 0, 0, 0, 0, 1, 0, 0, 1, 1]
    assert f.fp(5, 5, -7.0).to_float() == -7

def test_to_float():
    epsilon = 0.05
    assert f.fp(5, 5, 7.75).to_float() == 7.75
    assert f.fp(5, 5, 7.6).to_float() <= 7.6 + epsilon
    assert f.fp(5, 5, 7.6).to_float() >= 7.6 - epsilon
    assert f.fp(5, 5, -7.75).to_float() == -7.75
    assert f.fp(5, 5, -7.6).to_float() <= -7.6 + epsilon
    assert f.fp(5, 5, -7.6).to_float() >= -7.6 - epsilon
    
def test_add():
    assert (f.fp(5, 5, 7.75) + f.fp(5, 5, 2.5)).to_float() == 10.25
    assert (f.fp(5, 5, -7.75) + f.fp(5, 5, 2.5)).to_float() == -5.25
    assert (f.fp(5, 5, 7.75) + f.fp(5, 5, -2.5)).to_float() == 5.25
    assert (f.fp(5, 5, -7.75) + f.fp(5, 5, -2.5)).to_float() == -10.25

    with pytest.raises(Exception):
        assert f.fp(5, 5, -7.75) + f.fp(5, 7, -2.5)

    with pytest.raises(Exception):
        assert f.fp(11, 5, -7.75) + f.fp(5, 5, -2.5)

def test_mul():
    assert (f.fp(5, 5, 2.0) * f.fp(5, 5, 2.5)).to_float() == 5
    assert (f.fp(5, 4, 2.0) * f.fp(5, 4, -2.5)).to_float() == -5
    assert (f.fp(5, 4, -2.0) * f.fp(5, 4, 2.5)).to_float() == -5
    assert (f.fp(5, 5, -2.0) * f.fp(5, 5, -2.0)).to_float() == 4
    assert (f.fp(5, 5, -2.5) * f.fp(5, 5, -2.75)).to_float() == 6.875

def test_twos_compliment():
    fp = f.fp(5, 5, 2.75)
    fp.value = fp._twos_compliment(fp.value, 1)
    assert  fp.to_float() == -2.75
    fp.value = fp._twos_compliment(fp.value, 0)
    assert  fp.to_float() == -2.75

def test_set_value():
    fp = f.fp(10, 6)
    fp._set_value(-0.38539098534848615)
    assert fp.to_float() == -0.375
