import utility.logic as l
import pytest

@pytest.fixture
def fn_add():
    return lambda a, b : (a + b) % 2

@pytest.fixture
def fn_mul():
    return lambda a, b : (a * b) % 2

def test_or(fn_add, fn_mul):
    assert l.logic_or(0, 0, fn_add, fn_mul) == 0
    assert l.logic_or(0, 1, fn_add, fn_mul) == 1
    assert l.logic_or(1, 0, fn_add, fn_mul) == 1
    assert l.logic_or(1, 1, fn_add, fn_mul) == 1
    
def test_and(fn_mul):
    assert l.logic_and(0, 0, fn_mul) == 0
    assert l.logic_and(0, 1, fn_mul) == 0
    assert l.logic_and(1, 0, fn_mul) == 0
    assert l.logic_and(1, 1, fn_mul) == 1

def test_add3(fn_add):
    assert l.logic_add3(0, 0, 0, fn_add) == 0
    assert l.logic_add3(0, 0, 1, fn_add) == 1
    assert l.logic_add3(0, 1, 0, fn_add) == 1
    assert l.logic_add3(0, 1, 1, fn_add) == 0
    assert l.logic_add3(1, 0, 0, fn_add) == 1
    assert l.logic_add3(1, 0, 1, fn_add) == 0
    assert l.logic_add3(1, 1, 0, fn_add) == 0
    assert l.logic_add3(1, 1, 1, fn_add) == 1
