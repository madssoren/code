import utility.fp as f
import utility.exp_table as t
import utility.circuit as c
import utility.circuit_exp as e
import pytest

@pytest.fixture
def fn_add():
    return lambda a, b : (a + b) % 2

@pytest.fixture
def fn_mul():
    return lambda a, b : (a * b) % 2

def test_carry(fn_add, fn_mul):
    assert c.circuit_carry(0, 0, 0, fn_add, fn_mul) == 0
    assert c.circuit_carry(0, 0, 1, fn_add, fn_mul) == 0
    assert c.circuit_carry(0, 1, 0, fn_add, fn_mul) == 0
    assert c.circuit_carry(0, 1, 1, fn_add, fn_mul) == 1
    assert c.circuit_carry(1, 0, 0, fn_add, fn_mul) == 0
    assert c.circuit_carry(1, 0, 1, fn_add, fn_mul) == 1
    assert c.circuit_carry(1, 1, 0, fn_add, fn_mul) == 1
    assert c.circuit_carry(1, 1, 1, fn_add, fn_mul) == 1

def test_add(fn_add, fn_mul):
    assert c.circuit_add([1, 0, 1], [1, 1, 0], fn_add, fn_mul) == [0, 0, 0]
    assert c.circuit_add([1, 0, 1], [0, 1, 0], fn_add, fn_mul) == [1, 1, 1]
    assert c.circuit_add([1, 1, 1], [1, 1, 1], fn_add, fn_mul) == [0, 1, 1]

def test_exp_int(fn_add, fn_mul):
    epsilon = 0.05
    table = t.exp_indices(3, 3, 2.5)
    fp = f.fp(3, 3, 2)
    assert e.circuit_exp(fp, table, fn_add, fn_mul).to_float() == f.fp(4, 3, 6.25).to_float()

    table = t.exp_indices(3, 10, 2.5)
    fp = f.fp(3, 10, -2)
    assert e.circuit_exp(fp, table, fn_add, fn_mul).to_float() <= f.fp(4, 10, 0.16).to_float() + epsilon
    assert e.circuit_exp(fp, table, fn_add, fn_mul).to_float() >= f.fp(4, 10, 0.16).to_float() - epsilon

def test_exp_float(fn_add, fn_mul):
    epsilon = 0.05
    table = t.exp_indices(4, 4, 4)
    fp = f.fp(4, 4, 0.5)
    assert e.circuit_exp(fp, table, fn_add, fn_mul).to_float() == f.fp(4, 4, 2).to_float()

    table = t.exp_indices(4, 8, 2)
    fp = f.fp(4, 8, -0.5)
    assert e.circuit_exp(fp, table, fn_add, fn_mul).to_float() <= f.fp(5, 8, 0.7).to_float() + epsilon
    assert e.circuit_exp(fp, table, fn_add, fn_mul).to_float() >= f.fp(5, 8, 0.7).to_float() - epsilon

def test_exp_float2(fn_add, fn_mul):
    epsilon = 0.05
    table = t.exp_indices(8, 5, 2)
    fp = f.fp(8, 5, -2.5)
    assert e.circuit_exp(fp, table, fn_add, fn_mul).to_float() == f.fp(4, 4, 0.177).to_float()
