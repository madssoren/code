import utility.change_img as c
import utility.type_funcs as tf

def test_change_data():
    imgs = [([[1, 2], [3, 4]], 1), ([[5, 6], [7, 8]], 2)]
    assert c.change_data(imgs, tf.Float_funcs()) == [([[1, 2], [3, 4]], 1), ([[5, 6], [7, 8]], 2)]    
    assert c.change_data(imgs, tf.Fp_funcs(10, 10)) != [([[1, 2], [3, 4]], 1), ([[5, 6], [7, 8]], 2)]
