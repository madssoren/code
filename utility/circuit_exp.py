import utility.fp as f

def circuit_exp(fp, exp_table, fn_add, fn_mul):
    exps, int_size, float_size, i_comp = exp_table
    sign = fp.value[-1]
    identity_idx = float_size
    #print(fp.value)
    #print(i_comp)

    res = f.fp(int_size, float_size, 1)
    for i in range(0, fp.len - 1):
        temp_fp = f.fp(int_size, float_size)

        v_plus = fn_mul(fn_add(1, sign), fp.value[i])
        for j in exps[i]['i_plus']:
            temp_fp.value[j] = v_plus

        v_minus = fn_mul(sign, fn_add(1, fp.value[i]))
        for j in exps[i]['i_minus']:
            temp_fp.value[j] = v_minus

        for j in exps[i]['i_both']:
            temp_fp.value[j] = fn_add(v_plus, v_minus)

        temp_fp.value[identity_idx] = fn_add(1, fn_add(fp.value[i], sign))
        if (exps[i]['is_plus_identity'] == True):
            temp_fp.value[identity_idx] = fn_add(v_plus, temp_fp.value[identity_idx])

        if (exps[i]['is_minus_identity'] == True):
            temp_fp.value[identity_idx] = fn_add(v_minus, temp_fp.value[identity_idx])
            

        #print(temp_fp.to_float())
        res = res * temp_fp

    comp_fp = f.fp(int_size, float_size)
    for i in i_comp:
        comp_fp.value[i] = sign

    comp_fp.value[identity_idx] = fn_add(comp_fp.value[identity_idx], fn_add(1, sign))
    #print(comp_fp.to_float())

    res = res * comp_fp
    return res
