import utility.fp as f
import math as m
import fractions as fr

def exp_indices(int_num_bits, float_num_bits, base):
    size = int_num_bits + float_num_bits
    v = fr.Fraction(base) ** fr.Fraction((2 ** (int_num_bits - 2))) + 1
    new_int_size = m.ceil(m.log2(int(v) + 1)) + 1
    new_size = new_int_size + float_num_bits

    objs = []
    for n in range(0, size-1):
        exp_fp = f.fp(int_num_bits, float_num_bits)
        exp_fp.value[n] = 1

        val = fr.Fraction(base) ** fr.Fraction(exp_fp.to_float())
        val_int = int(val)
        val_frac = val - val_int

        temp = f.fp(new_int_size, float_num_bits, val_int)
        temp_frac = f.fp(new_int_size, float_num_bits, val_frac)
        for i in range(0, float_num_bits):
            temp.value[i] = temp_frac.value[i]

        #print(temp.to_float())
        i_plus = temp.value
        #print(temp.value)

        for i in range(n, size):
            exp_fp.value[i] = 1
        #print(exp_fp.value)
        temp2 = f.fp(new_int_size, float_num_bits, base ** exp_fp.to_float())
        #print(temp2.to_float())
        i_minus = temp2.value
        #print(i_minus)

        i_plus_set = set([])
        i_minus_set = set([])
        for i in range(0, new_size):
            if (i_plus[i] == 1):
                i_plus_set.add(i)

            if (i_minus[i] == 1):
                i_minus_set.add(i)

        i_both_set = i_plus_set & i_minus_set
        i_plus_set -= i_both_set
        i_minus_set -= i_both_set

        is_plus_identity = True if (i_plus[float_num_bits] == 1) else False
        is_minus_identity = True if (i_minus[float_num_bits] == 1) else False

        objs.append({
            'i_plus': list(i_plus_set),
            'i_minus': list(i_minus_set),
            'i_both': list(i_both_set),
            'is_plus_identity': is_plus_identity,
            'is_minus_identity': is_minus_identity
        })


    exp_fp = f.fp(int_num_bits, float_num_bits)
    exp_fp.value = [1]*size
    temp = f.fp(new_int_size, float_num_bits, base ** exp_fp.to_float())
        #print(temp.to_float())

    i_comp = []
    for i in range(0, new_size):
        if (temp.value[i] == 1):
            i_comp.append(i)

    return (objs, new_int_size + 1, float_num_bits, i_comp)
    
