import learning.helper as h
import learning.circuit as c
import utility.type_funcs as tf
import math as m
import operator as o
import functools as f
import pickle as p
import random as r
import time as t

class NeuralNet:
    def __init__(self, *args):
        if (len(args) == 1):
            self.type_funcs = tf.Float_funcs()
            self.load(args[0])
            return

        self._setup(args[0], args[1], args[2], args[3])

    def _setup(self, img_size, conv, output, rng):
        self.img_size = img_size
        self.kernel_size, self.channels, self.pooling_dim = conv
        self.output = output
        self.random = rng

        self._build_thetas()
        self._init_thetas()
        self.type_funcs = tf.Float_funcs()

    def _build_thetas(self):
        kern_x, kern_y = self.kernel_size
        self.theta_kernels = h.build_multi_dim([self.channels, kern_x, kern_y])

        img_x, img_y = self.img_size
        pool_x, pool_y = self.pooling_dim
        conv_x = img_x - kern_x + 1
        conv_y = img_y - kern_y + 1
        out_x = m.ceil(conv_x/pool_x)
        out_y = m.ceil(conv_y/pool_y)
        num_feats = self.channels * out_x * out_y

        self.theta_output = h.build_multi_dim([self.output, num_feats])

        self.pool_out_size = (out_x, out_y)
        self.conv_out_size = (conv_x, conv_y)

    def _init_thetas(self):
        for ed1 in self.theta_kernels:
            for ed2 in ed1:
                for i in range(0, len(ed2)):
                    ed2[i] = self.random()

        for ed1 in self.theta_output:
            for i in range(0, len(ed1)):
                ed1[i] = self.random()
        

    def load(self, fname):
        with open(fname, 'rb') as f:
            obj = p.load(f)
            self.img_size = obj['img_size']
            self.kernel_size = obj['kernel_size']
            self.channels = obj['channels']
            self.pooling_dim = obj['pooling_dim']
            self.output = obj['output']
            self.is_fn_bin = obj['is_fn_bin']
            self.theta_kernels = obj['theta_kernels']
            self.theta_output = obj['theta_output']
            self.pool_out_size = obj['pool_out_size']
            self.conv_out_size = obj['conv_out_size'] 

    def save(self, fname):
        with open(fname, 'wb') as f:
            obj = {
                'img_size': self.img_size, 
                'kernel_size': self.kernel_size, 
                'channels': self.channels, 
                'pooling_dim': self.pooling_dim,
                'output': self.output,
                'is_fn_bin': self.is_fn_bin,
                'theta_kernels': self.theta_kernels,
                'theta_output': self.theta_output,
                'pool_out_size': self.pool_out_size,
                'conv_out_size': self.conv_out_size
            }
            p.dump(obj, f)

    def change_type(self, type_funcs):
        self.type_funcs = type_funcs

        for ed1 in self.theta_kernels:
            for ed2 in ed1:
                for i in range(0, len(ed2)):
                    ed2[i] = self.type_funcs.init(ed2[i])

        for ed1 in self.theta_output:
            for i in range(0, len(ed1)):
                ed1[i] = self.type_funcs.init(ed1[i])

    def feed_forward(self, img, save_intermediate=False):
        feature_maps = []
        i = 0
        for kernel in self.theta_kernels:
            #print(f'Start convolve, #{i}')
            feature_maps.append(h.convolve(img, kernel))
            i+=1

        #print("Start relu")
        relus = []
        for feature_map in feature_maps:
            relus.append(h.relu(feature_map, self.type_funcs.max_zero)) 

        #print("Start pooling")
        pools = []
        for img in relus:
            pools.append(h.pooling(img, self.pooling_dim, self.type_funcs.mean))

        #print("Start softmax")
        features = f.reduce(o.concat, f.reduce(o.concat, pools))
        res = h.softmax(features, self.theta_output, self.type_funcs.exp)

        if (save_intermediate):
            return (res, feature_maps, relus, pools, features)
        return res

    def _delta_output(self, prediction, label):
        label_vec = [0]*self.output
        label_vec[label] = 1

        delta_output = []
        for i,lbl in enumerate(label_vec):
            delta_output.append(-(lbl-prediction[i]))
        return delta_output

    def _delta_features(self, features, delta_output):
        feature_gradients = []
        for f in features:
            feature_gradients.append(f*(1-f))

        delta_features = []
        for i in range(0, len(feature_gradients)):
            res = 0
            for j in range(0, self.output):
                res += self.theta_output[j][i] * delta_output[j]
            delta_features.append(res * feature_gradients[i])

        return delta_features

    def _delta_features_to_pools(self, delta_features):
        pool_out_x, pool_out_y = self.pool_out_size
        delta_pools = h.build_multi_dim([self.channels, pool_out_x, pool_out_y])
        for ed1 in delta_pools:
            for ed2 in ed1:
                for i in range(0, len(ed2)):
                    ed2[i] = delta_features.pop(0)
        return delta_pools

    def _upscale_delta_pools(self, delta_pools):
        pool_out_x, pool_out_y = self.pool_out_size
        conv_out_x, conv_out_y = self.conv_out_size
        upscaled_pools = h.build_multi_dim([self.channels, conv_out_x, conv_out_y])
        pool_x, pool_y = self.pooling_dim
        scale = 1/(pool_x * pool_y)
        for c in range(0, self.channels):
            for i in range(0, conv_out_x):
                pool_err_x = m.floor(i/pool_out_x)
                for j in range(0, conv_out_y):
                    pool_err_y = m.floor(j/pool_out_y)
                    # assumes mean pooling
                    upscaled_pools[c][i][j] = delta_pools[c][pool_err_x][pool_err_y] * scale 
        return upscaled_pools

    def _kernel_gradients(self, img, upscaled_pools):
        kernel_gradients = []
        for up in upscaled_pools:
            kernel_gradients.append(h.convolve(img, up))
        return kernel_gradients

    def _update_theta_output(self, alpha, features, delta_output):
        for i in range(0, self.output):
            for j in range(0, len(features)):
                self.theta_output[i][j] -= alpha * (delta_output[i] * features[j])

    def _update_theta_kernels(self, alpha, kernel_gradients):
        kern_x, kern_y = self.kernel_size
        for c in range(0, self.channels):
            for i in range(0, kern_x):
                for j in range(0, kern_y):
                    self.theta_kernels[c][i][j] -= alpha * (kernel_gradients[c][i][j])


    def backpropagation(self, img, label, alpha):
        prediction, _, _, _, features = self.feed_forward(img, True)
        prediction = prediction[0]/prediction[1]

        delta_output = self._delta_output(prediction, label)
        delta_features = self._delta_features(features, delta_output)
        delta_pools = self._delta_features_to_pools(delta_features)
        upscaled_pools = self._upscale_delta_pools(delta_pools)
        kernel_gradients = self._kernel_gradients(img, upscaled_pools)

        self._update_theta_output(alpha, features, delta_output)
        self._update_theta_kernels(alpha, kernel_gradients)

    def train(self, data, num_epoch, alpha, fname):
        ids = list(range(0, len(data)))
        total_count = num_epoch * len(data)
        count = 0

        start = t.process_time()
        for e in range(0, num_epoch):
            r.shuffle(ids)
            for i in ids:
                count += 1
                self.backpropagation(data[i][0], data[i][1], alpha)
                if (count % 100 == 0):
                    self.save(fname + '_' + str(len(data)) + '_' + str(e))

                    stop = t.process_time()
                    out = f'progress = {count}/{total_count}({count/total_count*100:.2f}%) - total time elapsed: {stop-start:.2f}s - remaining time: {(stop-start)/(count/total_count)*(1-count/total_count):.2f}s'
                    print(out)
                    with open('progress', 'w') as f:
                        f.write(out)
                    
            alpha *= 0.5

    def test(self, data):
        correct = 0
        incorrect = 0

        for i,l in data:
            pred = self.feed_forward(i)
            pred_lbl = pred.index(max(pred))
            if (pred_lbl == l):
                correct += 1
            else:
                incorrect += 1

        return (correct, incorrect)
