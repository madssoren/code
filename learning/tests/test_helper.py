import pytest
import learning.helper as h
import statistics as s
import math as m

def test_convolution():
    img = [[1,1,1,0,0],[0,1,1,1,0],[0,0,1,1,1],[0,0,1,1,0],[0,1,1,0,0]]
    kernel = [[1,0,1],[0,1,0],[1,0,1]]
    res = [[4,3,4],[2,4,3],[2,3,4]]
    assert h.convolve(img, kernel) == res

def test_pooling1():
    img = [[1,2,3],[4,5,6],[7,8,9]]
    #assert h.pooling(img, (2,2)) == [[5,6],[8,9]]
    assert h.pooling(img, (2,2), lambda x : max(x)) == [[5,6],[8,9]]
    assert h.pooling(img, (2,2), lambda x : s.mean(x)) == [[3,4.5],[7.5,9]]

def test_pooling2():
    img = [[1,1,1,0,0],[0,1,1,1,0],[0,0,1,1,1],[0,0,1,1,0],[0,1,1,0,0]]
    assert h.pooling(img, (2,2), lambda x : max(x)) == [[1,1,0],[0,1,1],[1,1,0]]

def test_relu():
    assert h.relu([[-1, 0], [1, 2]], lambda x : max([0, x])) == [[0, 0], [1, 2]]

def test_softmax():
    x_vec = [7, 9, 3]
    theta = [[1, 0.4, 2], [3, 2, 3.4], [7, 0, 0.1]]
    res_expected = [3.3015093240587016e-15, 0.4750208125210599, 0.5249791874789368]
    tuples = h.softmax(x_vec, theta, lambda x : m.exp(x))
    res = list(map(lambda x : x[0]/x[1], tuples))
    assert res  == res_expected

def test_build_multi_dim():
    dims1 = [3,1,2]
    dims2 = [3,1,0]
    dims3 = [2,3]
    dims4 = [2]
    assert h.build_multi_dim(dims1) == [[[0,0]], [[0,0]],[[0,0]]]
    assert h.build_multi_dim(dims2) == [[[]], [[]],[[]]]
    assert h.build_multi_dim(dims3) == [[0,0,0], [0,0,0]]
    assert h.build_multi_dim(dims4) == [0, 0]
