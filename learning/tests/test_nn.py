import pytest
import learning.nn as n
import learning.helper as h

def test_init_build_theta():
    img = (28,28)
    conv = ((9,9), 10, (2,2))
    out = 10
    rng = lambda : 0
    nn = n.NeuralNet(img, conv, out, rng)
    assert nn.theta_kernels == h.build_multi_dim([10,9,9])
    assert nn.theta_output == h.build_multi_dim([10, 1000])

def test_init_init_theta():
    img = (28,28)
    conv = ((2,2), 1, (10,10))
    out = 3
    rnglist = list(range(1,32))
    rng = lambda : rnglist.pop(0)
    nn = n.NeuralNet(img, conv, out, rng)

    assert nn.theta_kernels == [[[1,2],[3,4]]]
    assert nn.theta_output == [[5,6,7,8,9,10,11,12,13], [14,15,16,17,18,19,20,21,22], [23,24,25,26,27,28,29,30,31]]

@pytest.fixture
def nn_small():
    img_size = (6,6)
    conv = ((3,3), 4, (2,2))
    out = 4
    theta_kernel = [-8, 1, -1, 2, 6, 9, 3, 3, -5, 4, 3, 6, 0, -9, -9, 7, 6, 1, 3, 3, -9, -1, -5, 4, 4, -3, -6, -8, 7, -1, 3, 4, -5, 3, -2, 6]
    theta_out = [6, 7, 6, 8, 4, 1, 0, 0, 3, 7, 9, 8, 3, 9, 3, 5, 9, 0, 8, 4, 6, 0, 5, 1, 4, 0, 5, 1, 3, 3, 1, 3, 1, 4, 5, 4, 5, 5, 1, 2, 8, 6, 6, 8, 5, 8, 0, 9, 9, 6, 3, 4, 4, 4, 1, 1, 6, 7, 1, 3, 5, 7, 7, 7]
    theta_out = list(map(lambda x : x*0.00001, theta_out))
    rnglist = theta_kernel+theta_out
    rng = lambda : rnglist.pop(0)
    nn = n.NeuralNet(img_size, conv, out, rng)
    return nn

@pytest.fixture
def upscaled_pools():
    return [
    [[3/4, 3/4, 5/4, 5/4], [3/4, 3/4, 5/4, 5/4], [1/4, 1/4, 4/4 ,4/4], [1/4, 1/4, 4/4, 4/4]],
    [[2/4, 2/4, 1/4, 1/4], [2/4, 2/4, 1/4, 1/4], [3/4, 3/4, 4/4, 4/4], [3/4, 3/4, 4/4, 4/4]],
    [[3/4, 3/4, 1/4, 1/4], [3/4, 3/4, 1/4, 1/4], [2/4, 2/4, 2/4, 2/4], [2/4, 2/4, 2/4, 2/4]],
    [[1/4, 1/4, 5/4, 5/4], [1/4, 1/4, 5/4, 5/4], [2/4, 2/4, 3/4, 3/4], [2/4, 2/4, 3/4, 3/4]]
    ]

@pytest.fixture
def kernel_gradients():
    return [[[1484.5, 1456.5, 1342.5],[1630.75, 1323.5, 1281.25],[1999.75, 1623.25, 1696.75]],[[1354.5, 1231.0, 1258.0],[1316.75, 1163.25, 1333.5],[1477.25, 1286.0, 1316.75]],[[948.5, 909.75, 909.0], [938.75, 895.75, 1010.0], [1152.5, 1112.75, 1161.5]],[[1229.0, 1371.75, 1285.0],[1424.0, 1085.75, 1073.75],[1815.75, 1315.25, 1339.75]]]

def test_forward(nn_small):
    img = [[1,5,2,7,2,8],[1,2,6,2,6,8],[0,0,1,2,5,2],[1,1,1,0,4,2],[9,0,0,1,2,3],[2,4,1,6,2,0]]
    #assert nn_small.feed_forward(img) == [0.1, 0.1, 0.1, 0.7]
    
def test_delta_output(nn_small):
    prediction = [0.1, 0.2, 0.6, 0.1]
    label = 2
    assert nn_small._delta_output(prediction, label) == [0.1, 0.2, -0.4, 0.1]

def test_delta_features(nn_small):
    features = [3, 5, 1, 4, 2, 1, 3, 4, 3, 1, 2, 2, 1, 5, 2, 3]
    delta_output = [0.1, 0.2, 0.6, 0.1]
    # result verified by octave
    assert nn_small._delta_features(features, delta_output) == [-0.000234,-0.0007400000000000001,0.0,-0.000528,-0.00010000000000000002,0.0,-0.00010200000000000001,-0.00018000000000000004,-0.00039000000000000005,0.0,-0.000112,-0.000122,0.0,-0.0013999999999999998,-2.4000000000000004e-05,-0.0004320000000000001]

def test_delta_features_to_pools(nn_small):
    delta_features = [3, 5, 1, 4, 2, 1, 3, 4, 3, 1, 2, 2, 1, 5, 2, 3]
    assert nn_small._delta_features_to_pools(delta_features) == [[[3, 5], [1, 4]], [[2, 1], [3, 4]], [[3, 1], [2, 2]], [[1, 5], [2, 3]]]


def test_upscale_pools(nn_small, upscaled_pools):
    delta_pools = [[[3, 5], [1, 4]], [[2, 1], [3, 4]], [[3, 1], [2, 2]], [[1, 5], [2, 3]]]
    assert nn_small._upscale_delta_pools(delta_pools) == upscaled_pools
    

def test_kernel_gradients(nn_small, upscaled_pools, kernel_gradients):
    img = [[65, 253, 24, 32, 243, 115], [32, 10, 5, 209, 13, 27], [55, 223, 205, 58, 0, 125], [172, 10, 251, 248, 138, 189], [175, 129, 71, 103, 37, 185], [150, 255, 103, 248, 24, 183]]
    # result verified by octave
    assert nn_small._kernel_gradients(img, upscaled_pools) == kernel_gradients

def test_update_theta_output(nn_small):
    alpha = 0.5
    features = [3, 5, 1, 4, 2, 1, 3, 4, 3, 1, 2, 2, 1, 5, 2, 3]
    delta_output = [0.1, 0.2, 0.6, 0.1]
    nn_small._update_theta_output(alpha, features, delta_output)
    # result verified by octave
    assert nn_small.theta_output == [[-0.14994000000000002,-0.24993,-0.049940000000000005,-0.19992000000000001,-0.09996000000000001,-0.04999,-0.15000000000000002,-0.2,-0.14997000000000002,-0.04993,-0.09991,-0.09992000000000001,-0.04997,-0.24991,-0.09997,-0.14995000000000003],[-0.29991000000000007,-0.5,-0.09992000000000001,-0.39996000000000004,-0.19994,-0.1,-0.29995000000000005,-0.39999,-0.29996000000000006,-0.1,-0.19995000000000002,-0.19999,-0.09997,-0.49997,-0.19999,-0.29997000000000007],[-0.89999,-1.49996,-0.29995,-1.19996,-0.59995,-0.29995,-0.89999,-1.19998,-0.8999199999999999,-0.29994,-0.59994,-0.59992,-0.29995,-1.49992,-0.6,-0.8999099999999999],[-0.14991000000000002,-0.24994,-0.04997,-0.19996,-0.09996000000000001,-0.049960000000000004,-0.14999,-0.19999,-0.14994000000000002,-0.04993,-0.09999000000000001,-0.09997,-0.04995,-0.24993,-0.09993,-0.14993000000000004]]


def test_update_theta_kernels(nn_small, kernel_gradients):
    alpha = 0.5
    nn_small._update_theta_kernels(alpha, kernel_gradients)
    # result verified by octave
    assert nn_small.theta_kernels == [[[-750.25, -727.25, -672.25],[-813.375, -655.75, -631.625],[-996.875, -808.625, -853.375]],[[-673.25, -612.5, -623.0],[-658.375, -590.625, -675.75],[-731.625, -637.0, -657.375]],[[-471.25, -451.875, -463.5],[-470.375, -452.875, -501.0],[-572.25, -559.375, -586.75]],[[-622.5, -678.875, -643.5],[-709.0, -538.875, -541.875],[-904.875, -659.625, -663.875]]]

def test_back_propagation():
    pass
