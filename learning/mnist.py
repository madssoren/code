def bti(b):
    return int.from_bytes(b, byteorder='big')
    
def load_labels(path, num_load = None):
    labels = []
    with open(path, 'rb') as f:
        r = f.read(4)
        assert bti(r) == 2049

        num_labels = bti(f.read(4))
        load_num_labels = num_labels if num_load == None else min(num_load,num_labels)
        for i in range(0, load_num_labels):
            labels.append(bti(f.read(1)))

    return labels

def load_images(path, num_load = None):
    imgs = []
    with open(path, 'rb') as f:
        r = f.read(4)
        assert bti(r) == 2051

        num_imgs = bti(f.read(4))
        num_rows = bti(f.read(4))
        num_cols = bti(f.read(4))

        load_images = num_imgs if num_load == None else min(num_load,num_imgs)

        for i in range(0,load_images):
            img = []
            for r in range(0,num_rows):
                row = []
                for c in range(0,num_cols):
                    row.append(bti(f.read(1))/255.0)
                img.append(row)
            imgs.append(img)

    return imgs

def load_data(img_path, lbl_path, num_load = None):
    imgs = load_images(img_path, num_load)
    lbls = load_labels(lbl_path, num_load)

    return [(imgs[i], lbls[i]) for i in range(0, len(imgs))]
