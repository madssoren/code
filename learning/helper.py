import math as m
import fractions as f
import time as t

def build_multi_dim(dims):
    if (len(dims) < 2):
        return [0]*dims[0]

    temp = []
    dims = dims.copy()
    end = dims.pop(0)
    for lvl in range(0, end):
        temp.append(build_multi_dim(dims))

    return temp

def pooling(img, dim, fn):
    x, y = dim

    img_x = len(img)
    img_y = len(img[0])

    out_x_size = m.ceil(img_x/x)
    out_y_size = m.ceil(img_y/y)

    pool = build_multi_dim([out_x_size, out_y_size, 0])

    # fill pool
    for i in range(0, img_x):
        pool_x = m.floor(i/x)
        for j in range(0, img_y):
            pool_y = m.floor(j/y)
            pool[pool_x][pool_y].append(img[i][j])

    # apply t on pool elems
    for i in range(0, out_x_size):
        for j in range(0, out_y_size):
            pool[i][j] = fn(pool[i][j])

    return pool 

def convolve(img, kernel):
    kern_x_size, kern_y_size = len(kernel), len(kernel[0])
    out_x_size, out_y_size = len(img)-kern_x_size+1, len(img[0])-kern_y_size+1

    out = build_multi_dim([out_x_size, out_y_size])
    for i in range(0, out_x_size):
        for j in range(0, out_y_size):
            for k in range(0, kern_x_size):
                for l in range(0, kern_y_size):
                    res = img[i+k][j+l] * kernel[k][l]
                    if (out[i][j] == 0):
                        out[i][j] = res
                    else:
                        out[i][j] += res

                        

    return out

def relu(feature_map, fn):
    relu = []
    for col in feature_map:
        relu.append([fn(r) for r in col])

    return relu

e = f.Fraction(m.exp(1))

def inner_product(theta_vec, x_vec):
    assert len(theta_vec) == len(x_vec)

    inner = theta_vec[0] * x_vec[0]
    for i in range(1, len(x_vec)):
        inner += theta_vec[i] * x_vec[i]

    return inner

def int_sum_ei_fn(exp_inner_sum, exp_inner):
    return exp_inner_sum + exp_inner

def int_div_ei_fn(exp_inner, exp_inner_sum):
    return float(exp_inner / exp_inner_sum)

def softmax(x_vec, theta_matrix, fn_exp):
    inner_prods = []
    for theta_vec in theta_matrix:
        inner_prods.append(inner_product(theta_vec, x_vec))

    #return inner_prods
    exp_inners = []
    for inner in inner_prods:
        exp_inners.append(fn_exp(inner))

    exp_inner_sum = exp_inners[0]
    for exp_inner in exp_inners[1:]:
        exp_inner_sum += exp_inner

    res = []
    for exp_inner in exp_inners:
        res.append((exp_inner, exp_inner_sum))

    return res
