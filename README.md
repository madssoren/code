The repo contains a prototype that was developed as part of a master's thesis.

The implementation of the cryptographic scheme is located in `/crypto/`, whereas the network and data functions are located at `/learning/`.
Circuit implementations are located at `/utility/`, since they are shared by the network and crypto code.
The root of the repo contains a variety of files used to train the network and conduct our experiments.

All four paths contain a test directory, that require `pytest` to run.
Invocation of `make test` from `/` runs all of the tests.
